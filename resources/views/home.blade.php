@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Portal</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="/posts/create" class="btn-lg btn-outline-secondary" style="float:right;">New Post</a>
                    <h3>Your Posts</h3>
                    @if(count($posts) > 0)
                    <table class="table table-striped">
                      <tr>
                        <th>Title</th>
                        <th></th>
                      </tr>
                      @foreach($posts as $post)
                        <tr>
                          <td><a href="/posts/{{$post->id}}" class="btn-sm btn-default">{{$post->title}}</a></td>
                          <td>{!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'style' => 'float:right'])!!}
                             {{Form::hidden('_method', 'DELETE')}}
                             {{Form::submit('Delete', ['class' => 'btn-sm btn-outline-danger'])}}
                             {!!Form::close()!!}</td>
                        </tr>
                      @endforeach
                    </table>
                  @else
                    <p>You dont have any posts yet. . .</p>

                  @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
