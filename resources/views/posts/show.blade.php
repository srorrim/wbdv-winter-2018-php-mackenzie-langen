@extends('layouts.app')

@section('content')

<div class="container">
      <div class="card">
        <img class="card-img-top" src="/storage/cover_images/{{ $post->cover_image }}" alt="Card image cap">
        <div class="card-body">
          <h2 class="card-title">{{ $post->title }}</h2>
          <p class="card-text">{!!$post->body!!}</p>
          <small>Written on {{$post->created_at}} by {{$post->user->name}}</small><br/>
          @if(!Auth::guest())
            <a href="/posts" class="btn-sm btn-outline-secondary" id="postReturn" role="button">Return</a>
            @if(Auth::user()->id == $post->user_id)
              {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'style' => 'float:right'])!!}
                 {{Form::hidden('_method', 'DELETE')}}
                 {{Form::submit('Delete', ['class' => 'btn-sm btn-outline-danger'])}}
              {!!Form::close()!!}
            @endif
          @endif
        </div>
        <div class='card-body'>
          @foreach($post->comments as $comment)
            <div class='comment'>
              <hr>
              <p><strong>Name:</strong> {{ $comment->post->user->name }}</p>
              <p>{{ $comment->comment }}</p>
            </div>
          @endforeach
        </div>

  </div>


{{-- Comment form --}}


@if(Auth::user())
  <div class="card">
    <div id='comment-form'>
      {{ Form::open(['route' => ['comments.store', $post->id], 'method' => 'POST']) }}
        {{ Form::label('comment', 'Comment:') }}
        {{ Form::textarea('comment', null, ['class' => 'form-control', 'rows' => '5']) }}
        {{ Form::submit('Add Comment', ['class' => 'btn-sm btn-outline-secondary']) }}
      {{ Form::close() }}
    </div>
  </div>
@endif
</div>
  @endsection
