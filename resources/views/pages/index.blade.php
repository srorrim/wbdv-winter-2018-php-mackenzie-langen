@extends('layouts.app')

@section('home-content')
<div class='hero-image'>
    <div class="hero-text">
      <h1>SRORRIM</h1>
      <h5>A collection of my thoughts, and ideas</h5>
      <p>
          <a class='btn-light btn-lg' href="/posts/create" role="button">Create</a>
          <!-- <a class='btn-light btn-lg' href="/register" rolde="button">Sign up</a> -->
      </p>
    </div>
</div>

@endsection
