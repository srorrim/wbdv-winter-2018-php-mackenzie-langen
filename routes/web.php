<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Get
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/home', 'HomeController@index')->name('home');


//Post
Route::post('comments/{post_id}', ['uses' => 'CommentsController@store', 'as' =>'comments.store']);


//Resources
Route::resource('posts', 'PostsController');

//Vaildation
Auth::routes();
