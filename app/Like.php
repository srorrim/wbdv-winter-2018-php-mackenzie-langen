<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model{
  morphedByMany('App\Comment', 'likeable');
  }

  /**
   * Get all of the posts that are assigned this like.
   */
  public function posts(){
      return $this->morphedByMany('App\Post', 'likeable');
  }
}
}
